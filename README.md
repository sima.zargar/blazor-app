
## To create the blazor-app

dotnet new blazorwasm-empty -o blazor-app

## To create the react-app

npx create-react-app react-app

## To run blazor-app
dotnet run
Visit http://localhost:5050

## To publish Blazor output to React

dotnet add package microsoft.aspnetcore.components.customelements
dotnet publish

Then copy the 2 below folders to react-app/public/
blazor-app/publish/wwwroot/_content
blazor-app/publish/wwwroot/_framework

The two below scripts are added to index.html in React
<script src="_framework/blazor.webassembly.js"></script>
<script src="_content/Microsoft.AspNetCore.Components.CustomElements.lib.module.js"></script>

## To run react-app
npm start
Visit http://localhost:3000

